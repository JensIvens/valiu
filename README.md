## Blockchain product philosophy

To explain my product philosophy, I want to talk about two different topics.

**Human risk of building crypto products**

When thinking and talking about building blockchain products and what the challenges are, the first thing that always comes to mind is the technical risk. And it's true, we're facing a lot of technical challenges. But technical risk isn't the biggest risk of why crypto products might fail. The biggest risk is the human risk. And no one is talking about it. We (as an industry) should start putting ourselves in our user&#39;s shoes first or we'll lose track of who we're building for. The tech is hard enough and sometimes intimidating enough that it's easy to let people slip through the cracks. We have created the system that way and now a lot of developers decide to stick to webdevelopment after looking into smart contracts, or for someone to see consistent 9% yield and just leave because they don't know how to set up a wallet or deal with taxes.

But what's the solution? First of all, we have to adapt our personas, and build with a wider group of personas in mind to make sure we're not accidentally exclusionary. Next to personas, we should adapt our user stories and make them more inclusive. Focus on the least technical potential user, and write our internal stories in their language, with words they understand. And also include them in our testing. "We tested it, it works" doesn't mean that it works for our users. Including them in the entire product life cycle is more important than ever.

Way too often, we build products that serve us as a community, but where the barriers to entry are too high for people with different technical backgrounds than us. This is a super important point, especially for this job.

**Incentives vs Motivations**

Most crypto products are community driven and we focus on "incentivizing engagement". But that's a poor way of phrasing it, we should focus on "motivating engagement". Incentives are an extrinsic tool used to create motivation, we should focus on intrinsic tools instead.

Why do make that mistake? We assume that incentives are the primary driver of motivation, but that's wrong. Incentives are only a part of our motivations. A second mistake that we make is that we assume that we can create incentive programs to boost engagement. The majority of studies on incentives and motivations are focused on employers & employees, but there are a few things we can learn from them. ["Research suggests that, by and large, rewards succeed at securing one thing only: temporary compliance. When it comes to producing lasting change in attitudes and behavior, rewards, like punishment, are strikingly ineffective."](https://hbr.org/1993/09/why-incentive-plans-cannot-work) Rewards undermine interest, reduce cooperation, and discourage creativity, especially for complex tasks. We need to innovate beyond incentives which are narrowly focused on manipulating through reward/punishment paradigms. Instead, we should center "motivation programs" around intrinsic drivers like belonging, challenge, and cooperation.

@jensivens